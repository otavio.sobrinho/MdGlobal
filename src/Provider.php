<?php

namespace LocknLoad\MdGlobal;

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__. '/models/GbFiltro.php';
        include __DIR__. '/models/GbImage.php';
        include __DIR__. '/models/ScMenu.php';
        include __DIR__. '/models/ScNivel.php';

        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {


     }
}
