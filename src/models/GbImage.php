<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;

class GbImage extends ModelCore
{

    use SoftDeletes;

    protected $table = 'gb_imagens';
    protected $softDelete = true;

    public function imgoutput(){
        return "/assets/site/".env("APP_ENV")."/images/";
    }

    public function produtos()
    {
        return $this->belongsToMany('EcProduto', 'gb_imagens_has_ec_produtos', 'gb_imagens_id', 'ec_produtos_id');
    }
    public function presentation(){
        return $this->nome;
    }
}
