<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbCidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_cidades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_gb_estado')->unsigned()->index('gb_cidades_id_gb_estado_foreign');
			$table->string('cidade', 100);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_cidades');
	}

}
